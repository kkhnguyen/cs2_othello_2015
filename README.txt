Implemented alpha-beta pruning to speed up the selection of a move.

Added a bias to moves to corners and moves to edges, and a negative bias to moves to positions adjacent to corners, to increase chances of winning.

Max depth of decision tree set to 6 to see a few steps ahead of opponent.

Optimized compiler to -O3.