#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

#include <algorithm>

#define MAXDEPTH 6
#define ABORTTIME 1000

class Player {

private:
    
    Side playerSide;

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    Board playerBoard;
};

#endif
