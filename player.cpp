#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    playerBoard = Board(); 
    playerSide = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	
}


int minimax(Board *board, Side side, int depth)
{
	Side next = (side == BLACK) ? WHITE : BLACK;
	
	if (depth >= MAXDEPTH)
	{
		return board->count(side);
	}
	
	int result = -65;
	
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			
			Move move(i,j);
			if (board->checkMove(&move, next))
			{				
				Board *testBoard = board->copy();
				testBoard->doMove(&move, next);
				
				int score = minimax(testBoard, next, depth + 1);
				if (next == side)
				{					
					if (score > result || result == -65)
					{
						result = score;
					}
				}
				else
				{
					if (score < result || result == -65)
					{
						result = score;
					}
				}
				delete testBoard;
			}
		}
	}
	
	return result;
}

int alphabeta(Board *board, Side side, int depth, int alpha, int beta)
{
	Side next = (side == BLACK) ? WHITE : BLACK;
	
	if (depth == 0)
	{
		return board->count(side);
	}
	
	if (board->hasMoves(side) == false)
	{
		if (board->hasMoves(next) == false)
		{
			return board->count(side);
		}
		int val = -alphabeta(board, next, depth - 1, -beta, -alpha);
		if (val >= beta)
		{
			return val;
		}
		if (val > alpha)
		{
			alpha = val;
		}
	}
	
	else
	{
	    for (int i = 0; i < 8; i++)
    	{
	    	for (int j = 0; j < 8; j++)
	    	{			
	    		Move move(i,j);
	    		if (board->checkMove(&move, side))
	    		{				
	    			Board *testBoard = board->copy();
	    			testBoard->doMove(&move, side);
				
	    			int val = -alphabeta(testBoard, next, depth - 1, -beta, -alpha);
	    			if (val >= beta)
	    			{
	    				return val;
	    			}
	    			if (val > alpha)
		    		{
		    			alpha = val;
		    		}
	    			delete testBoard;
	    		}
	    	}
	    }
	}
	
	return alpha;
}	
			

bool isOnCorner(Move *move)
{
	int x = move->getX();
	int y = move->getY();
	
	if ((x == 0 && y == 0) || (x == 7 && y == 7) || (x == 7 && y == 0) || (x == 0 && y == 7))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isAdjCorner(Move *move)
{
	int x = move->getX();
	int y = move->getY();
	
	if ((x == 0 && y == 1) || 
	    (x == 1 && y == 0) || 
	    (x == 1 && y == 1) || 
	    (x == 0 && y == 6) || 
	    (x == 1 && y == 6) || 
	    (x == 1 && y == 7) ||
	    (x == 6 && y == 0) || 
	    (x == 6 && y == 1) || 
	    (x == 7 && y == 1) ||
	    (x == 6 && y == 6) || 
	    (x == 6 && y == 7) || 
	    (x == 7 && y == 6))  
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool isOnEdge(Move *move)
{
	int x = move->getX();
	int y = move->getY();
	
	if ((x == 0 && 2 <= y && y <= 5) ||
	    (y == 0 && 2 <= x && x <= 5) ||
	    (x == 7 && 2 <= y && y <= 5) ||
	    (y == 7 && 2 <= x && x <= 5))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    Side other = (playerSide == BLACK) ? WHITE : BLACK;
    playerBoard.doMove(opponentsMove, other);
    
    /** Simple Player
    if (playerBoard.hasMoves(playerSide))
    {
		std::cerr << "Player can make move." << std::endl;
		Move *playersMove = NULL;
		for (int i = 0; i < 8; i++)
		{
            for (int j = 0; j < 8; j++)
            {
                Move move(i, j);
                if (playerBoard.checkMove(&move, playerSide))
                {
					playersMove = new Move(i, j);
					playerBoard.doMove(playersMove, playerSide);
					std::cerr << "Player made move: " << playersMove->getX() << ", " << playersMove->getY() << std::endl;
					return playersMove;
				}
			}
		}
	}
	*/
	
	/** Heuristic Player
	if (playerBoard.hasMoves(playerSide))
    {
		Move *playersMove = NULL;
		int bestMoveX = -1;
		int bestMoveY = -1;
		int bestScore = -999;
				
		for (int i = 0; i < 8; i++)
		{
            for (int j = 0; j < 8; j++)
            {              
                Move move(i, j);
                if (playerBoard.checkMove(&move, playerSide))
                {
					std::cerr << "Checking Move " << i << ", " << j << std::endl;
					Board *dupeBoard = playerBoard.copy();
					dupeBoard->doMove(&move, playerSide);
					int score = dupeBoard->count(playerSide);
					
					if (isOnCorner(&move))
		        	{
		        		score += 10;
		        	}			
	        		if (isAdjCorner(&move))
		        	{
		        		score -= 5;
		        	}
	        		if (isOnEdge(&move))
		        	{
		        		score += 5;
		        	}
					
					if (score > bestScore)
					{		
					    bestMoveX = i;
					    bestMoveY = j;
					    bestScore = score;
					}
					
					delete dupeBoard; 
				}
			}
		}
		
		if (bestMoveX != -1 && bestMoveY != -1)
		{
			playersMove = new Move(bestMoveX, bestMoveY);
			playerBoard.doMove(playersMove, playerSide);
		    return playersMove;
		}
		else
		{
			return NULL;
		}
	}
	*/

    /** Minimax Player
	
	if (playerBoard.hasMoves(playerSide))
    {	
		int bestMoveX = -1;
		int bestMoveY = -1;
		int bestScore = -65;
		int score;
		
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Move move(i,j);
				if (playerBoard.checkMove(&move, playerSide))
                {
					Board *testBoard = playerBoard.copy();
					testBoard->doMove(&move, playerSide);
			
					if (testingMinimax == true)
					{
						score = minimax(testBoard, playerSide, MAXDEPTH - 1);
					}
					else
					{			
						score = minimax(testBoard, playerSide, 1);
			
						if (isOnCorner(&move))
						{
							score += 10;
						}			
						if (isAdjCorner(&move))
						{
							score -= 5;
						}
						if (isOnEdge(&move))
						{
							score += 5;
						}
					}
			
					if (score > bestScore || bestScore == -65)
					{
						bestMoveX = i;
						bestMoveY = j;
						bestScore = score;
					}
					delete testBoard;
				}
			}
		}

		if (bestMoveX != -1 && bestMoveY != -1)
		{
			Move *bestMove = new Move(bestMoveX, bestMoveY);
			playerBoard.doMove(bestMove, playerSide);
			return bestMove;
		}			
	}		
    */
    
    /** Alpha-beta Pruning */
    
    if (playerBoard.hasMoves(playerSide))
    {	
        int alpha = -65;
    	int beta = 65;
    	int bestMoveX = -1;
    	int bestMoveY = -1;
	
    	for (int i = 0; i < 8; i++)
    	{
    		for (int j = 0; j < 8; j++)
    		{
	    		Move move(i,j);
	    		if (playerBoard.checkMove(&move, playerSide))
	    		{				
	    			Board *testBoard = playerBoard.copy();
	    			testBoard->doMove(&move, playerSide);
	    			int val = -alphabeta(testBoard, other, MAXDEPTH-1, -beta, -alpha);
				
				    if (isOnCorner(&move))
					{
						val += 10;
					}			
					if (isAdjCorner(&move))
					{
						val -= 5;
					}
					if (isOnEdge(&move))
					{
						val += 5;
					}
				
	    			if (val >= beta)
	    			{
	    				Move *bestMove = new Move(i, j);
	    				playerBoard.doMove(bestMove, playerSide);
	    				return bestMove;
	    			}
	    			if (val > alpha)
	    			{
	    				alpha = val;
	    				bestMoveX = i;
	    				bestMoveY = j;
	    			}
	    			delete testBoard;
	    		}
	    	}
	    }
	
	    if (bestMoveX != -1 && bestMoveY != -1)
	    {
	        Move *bestMove = new Move(bestMoveX, bestMoveY);
	        playerBoard.doMove(bestMove, playerSide);
	        return bestMove;
	    }
    }
    return NULL;

}


